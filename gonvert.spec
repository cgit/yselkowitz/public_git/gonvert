Name:           gonvert
Version:        0.2.39
Release:        9%{?dist}
Summary:        Units conversion utility
License:        GPL+
URL:            http://unihedron.com/projects/gonvert
Source0:        http://unihedron.com/projects/gonvert/downloads/%{name}-%{version}.tar.gz
Source1:        gonvert.ui
Patch0:         gonvert-desktop-icon.patch
Patch1:         gonvert-gtkbuilder.patch

BuildRequires:  desktop-file-utils
BuildRequires:  make
BuildRequires:  python2-devel
BuildRequires:  /usr/bin/pathfix.py
Requires:       python2
Requires:       pygtk2

BuildArch:      noarch

%description
gonvert is a conversion utility that allows conversion between many units
like CGS, Ancient, Imperial with many categories like length, mass, numbers,
etc. All units converted values shown at once as you type. Easy to add/change
your own units. Written in Python, pygtk, libglade.


%prep
%setup -q

%patch0 -p0
%patch1 -p0
cp %{SOURCE1} .

# remove execute bits from doc files
chmod -x doc/*

# fix encoding on THANKS and TODO files
pushd doc >/dev/null
iconv -f iso8859-1 -t utf-8 THANKS > THANKS.conv && mv -f THANKS.conv THANKS
iconv -f iso8859-1 -t utf-8 TODO > TODO.conv && mv -f TODO.conv TODO
popd >/dev/null


%build
# there's nothing that needs built


%install
make DESTDIR=${RPM_BUILD_ROOT} prefix=%{_prefix} install
rm -rf ${RPM_BUILD_ROOT}/%{_docdir}/%{name}

desktop-file-install                                \
    --delete-original                               \
    --remove-category Application                   \
    --dir ${RPM_BUILD_ROOT}%{_datadir}/applications \
    ${RPM_BUILD_ROOT}%{_datadir}/gnome/apps/Utilities/gonvert.desktop

pathfix.py -pni "%{__python2} %{py2_shbang_opts}" $RPM_BUILD_ROOT%{_bindir}/gonvert

%files
%license doc/COPYING
%doc doc/CHANGELOG doc/FAQ doc/README doc/THANKS doc/TODO
%{_bindir}/gonvert
%{_datadir}/applications/%{name}.desktop
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/gonvert
%{_datadir}/pixmaps/gonvert*.png


%changelog
* Sun Aug 02 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 0.2.39-9
- Port to GtkBuilder, drop libglade dependency

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.39-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.39-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Mon Sep 10 2018 Gwyn Ciesla <limburgher@gmail.com> - 0.2.39-6
- Fix shebang handling.

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.39-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.39-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.39-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.39-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Aug 30 2016 Jon Ciesla <limburgher@gmail.com> - 0.2.39-1
- Latest upstream.

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.38-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jan 15 2016 Jon Ciesla <limburgher@gmail.com> - 0.2.38-1
- Latest upstream.

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.36-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.36-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri May 30 2014 Richard Hughes <richard@hughsie.com> - 0.2.36-1
- Latest upstream.

* Thu May 29 2014 Richard Hughes <richard@hughsie.com> - 0.2.35-1
- Latest upstream.

* Fri Jan 31 2014 Jon Ciesla <limburgher@gmail.com> - 0.2.34-1
- Latest upstream.
- Date fixups.

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.32-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Mon Feb 11 2013 Jon Ciesla <limburgher@gmail.com> - 0.2.32-1
- Latest upstream.
- Drop desktop vendor tag.

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.25-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.25-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.25-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Jan 26 2011 Jon Ciesla <limb@jcomserv.net> - 0.2.25-1
- New upstream.

* Thu May 13 2010 Jon Ciesla <limb@jcomserv.net> - 0.2.24-1
- New upstream.

* Tue Mar 30 2010 Jon Ciesla <limb@jcomserv.net> - 0.2.23-1
- New upstream.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.20-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Apr 27 2009 Jon Ciesla <limb@jcomserv.net> - 0.2.20-1
- New upstream.

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.19-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Dec 04 2007 Jon Ciesla <limb@jcomserv.net> - 0.2.19-3
- Re-corrected license tag. :)

* Tue Dec 04 2007 Jon Ciesla <limb@jcomserv.net> - 0.2.19-2
- Correct license tag, description typo, desktop icon name.

* Tue Oct 16 2007 Todd Zullinger <tmz@pobox.com> - 0.2.19-1
- update to 2.19
- update License and URL tags
- drop version from python Requires
- replace libglade2 and pygtk2 Requires with pygtk2-libglade
- remove execute bits from doc files
- remove unneeded make in %%build
- re-work %%install, avoid placing glade file under python site dir (#237286)
- fix encoding on THANKS and TODO files

* Thu Dec 14 2006 Jason L Tibbitts III <tibbs@math.uh.edu> - 0.2.15-4
- Bump and rebuild.

* Tue Dec 12 2006 Paul F. Johnson <paul@all-the-johnsons.co.uk> 0.2.15-3
- rebuild against new python

* Mon Jul 3 2006 Paul F. Johnson <paul@all-the-johnsons.co.uk> 0.2.15-2
- fixed the glade file problem

* Fri Apr 7 2006 Paul F. Johnson <paul@all-the-johnsons.co.uk> 0.2.15-1
- Bump to 0.2.15
- spec file altered

* Fri Apr  8 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sat Nov 27 2004 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.2.11-1
- Bump release/Spec Cleanup.

* Thu Oct 21 2004 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.2.11-0.fdr.1
- 2.11.

* Sat Aug 28 2004 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.2.10-0.fdr.1
- 2.10.

* Thu Aug 12 2004 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.2.07-0.fdr.1
- 2.07.

* Thu Feb 05 2004 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.1.9-0.fdr.1
- Update to 0.1.9.

* Sun Jan 25 2004 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.1.7-0.fdr.1
- Update to 0.1.7.

* Sun Nov 16 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.1.6-0.fdr.3
- BuildReq desktop-file-utils.

* Tue Aug 05 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.1.6-0.fdr.2
- Corrected file permissions.
- Corrects path of Source0.

* Wed Jul 30 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.1.6-0.fdr.1
- Fedorafication.

* Sun Jun 29 2003 Dag Wieers <dag@wieers.com> - 0.1.6-0
- Updated to release 0.1.6.

* Wed Feb 26 2003 Dag Wieers <dag@wieers.com> - 0.1.5-0
- Initial package. (using DAR)
